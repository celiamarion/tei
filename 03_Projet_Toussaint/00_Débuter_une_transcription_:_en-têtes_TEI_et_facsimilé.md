## Débuter une transcription : utiliser le `modèle de fichier`

Pour chaque facsimilé un fichier XML-TEI est produit et contient la transcription de ce facsimilé.

1. Choisir un facsimilé parmi ceux qui vous ont été attribués. Par exemple, le facsimilié **`LRT_01_01_00056.jpg`**.

1. Sous Oxygen, ouvrir le modèle de fichier et l'enregistrer sous un nouveau nom de la forme **``XXX``**

Le modèle de fichier est disponible <rouge>ICI</rouge>.


## Renseigner les informations concernant la personne qui transcrit : le `TEI Header`

Dans l'entête du fichier, la balise <teiHeader> contient des informations pré-renseignées. Certaines balises sont à laisser telles quelles, d'autres sont à compléter.

Le bloc de balises concernées est `<resp>` contenu dans la balise `<teiHeader>`.

Deux informations sont à renseigner :
* le nom du transcripteur ou de la transcriptrice : balise `<name>`
* la date de la transcription : attribut `@when` de la balise `<resp>`

```xml
<teiHeader>
  (...)
  ... Réticence, Toussaint... <!-- Ne pas modifier -->
  <resp when="XXX"> <!-- Indiquer la date de transcription sous la forme AAAAMMDD (par exemple, 20180430 pour le 30 avril 2018)-->
    <name>XXX</name><!-- Indiquer le nom du transcripteur -->
  </resp>
  (...)
</teiHeader>
```

## Indiquer le facsimilé transcrit : balise `<surface>`
<rouge>(...)</rouge>

```xml
(...)
<surface>XXX</surface> <!-- Indiquer ici le nom complet du fichier image avec son extension (.jpg)-->
```

## Transcrire les contenus
Une fois les éléments d'entêtes et concernant le facsimilé renseignés, vous pouvez passer à la phase de transcription à proprement parlée.

Voir la page dédiée : [Transcription des contenus](Transcription_des_contenus).