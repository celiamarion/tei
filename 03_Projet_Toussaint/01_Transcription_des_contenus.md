[exemple1]: img/LRT_01_01_00056_72dpi.jpg
[TOC]


## Numérotations des pages : `<fw>`
La numérotation ajoutée pour l'inventaire se trouve en haut à droite de l'image.
<!--![Exemple1][exemple1]-->
<!--<p><img src="img/LRT_01_01_00056_72dpi.jpg" style="float:right; width: 300px"/></p>-->

<p><img src="img/LRT_01_01_00056_72dpi.jpg" style="width: 300px"/></p>

```xml
<fw place="top" type="num_page">NUMÉRO</fw>
```

L'élément à utiliser est `<fw>`.

Les attributs à renseigner sont :
* `@place` : valeur par défaut, `top`
* `@place` : valeur par défaut, `num_plage`
* `@rend` : ne pas renseigner


## Transcrire un paragraphe : `<p>`
Le contenu d'un paragraphe doit être transcrit au sein de la balise `<p>`

```xml
<p>
<!-- Transcrire le paragraphe ici -->
</p>
```

## Transcrire un paragraphe avec alinéa : `<p @rend="indent">`
<rouge>mettre une image</rouge>

Le paragraphe doit être transcrit - comme tout paragraphe - au sein de la balise `<p>`. Mais on spécifiera un attribut à cette balise :
* attribut `@rend` dont la valeur doit être `indent`

```xml
<p rend="indent">
<!-- Transcrire le paragraphe ici -->
</p>
```

## Transcrire une rature : `@rend` de `<del>`
<rouge>mettre une image</rouge>

Au sein d'un paragraphe, une rature est transcrite avec l'élément `<del>`.
C'est l'attribut `@rend` qui permet d'indiquer comment cette rature prend forme :
* rature manuelle ou texte barré à la main : `@rend` prend la valeur `line-through`
* rature machine ou texte barré à la machine à écrire : `@rend` prend la valeur `overtyped`

```xml
<p rend="indent">
   (...)
   phrase contenant <del rend="line-through">une rature manuelle</del>
   (...)
   phrase contenant <del rend="overtyped">une rature machine</del>
   (...)
</p>
```


## Zone illisible ou difficilement déchiffrable : (`<gap>` ou ) `<unclear>`
<rouge>??? Un passage du texte (lettre, mot, suite de mots...) illisible, indéchiffrable est transcrit avec l'élément ??`<gap>` / <unclear/>??.</rouge>

Un passage du texte difficilement lisible mais pour lequel il est possible d'inférer ou de supposer une transcription est transcrit avec l'élément `<unclear>`.

```xml
<p rend="indent">
   (...)
   une phrase avec une portion illisible est annotée ainsi : <unclear>indiquer ici la lecture incertaine proposée</unclear>.
   (...)
   une phrase avec une portion illisible pour laquelle on ne propose pas de lecture incertaine est annotée ainsi : <unclear /> <gap/>.
   (...)
</p>
```

## Transcrire un soulignement : `<hi @rend="underline">`
<rouge>mettre une image</rouge>

Au sein d'un paragraphe, un passage souligné est transcrit avec l'élément `<hi>` qui porte l'attribut `@rend` dont la valeur doit être `underline`.

```xml
<p rend="indent">
   (...)
   phrase contenant <hi rend="underline">du texte souligné</del>
   (...)
</p>
```
