[exemple1]: img/LRT_01_01_00056_72dpi.jpg
[TOC]


## Transcrire les ajouts : `<add>`
Tous les ajouts (portions de texte ajoutés à la main par l'auteur au feutre noir) doivent être indiqués avec la balise `<add>`.

*Rem. Cela exclut les numéros de page qui sont eux aussi "ajoutés" mais pas par l'auteur. Pour en savoir plus sur leur transcription, voir [Numérotations des pages](Transcription_des_contenus#page_Numerotations-des-pages)*

* Si un ajout est ancré dans le texte, c'est-à-dire que l'auteur a indiqué d'une façon ou d'une autre où celui-ci s'insère, l'ajout est transcrit à l'endroit même où il s'insère dans le texte tapuscrit (et non pas là où l'ajout est disposé physiquement sur le feuillet)
  * l'élément `<add>` est complété par deux attributs :
    * l'attribut `@type` qui prend la valeur `addition`
    * l'attribut `@place` qui prend la valeur `inline`
* Si l'ajout est indépendant du texte principal, c'est-à-dire qu'il n'est pas lié ou ancré au texte tapuscrit, il est transcrit à son emplacement propre. <rouge>??Par principe, on transcrit d'abord les ajouts en haut de page, puis le contenu principal, puis les ajouts en bas de page.??</rouge>
  * l'élément `<add>` est complété par deux attributs :
    * l'attribut `@type` qui prend la valeur `marginale-note`
    * l'attribut `@place` qui prend l'une des valeurs suivantes :
      * si l'ajout est avant le contenu principal, valeur `top`
      * si l'ajout est après le contenu principal, valeur `bottom`

```xml
(...)
```



## Transcrire un déplacement
Un déplacement ...

```xml
<p>
<!-- Transcrire le paragraphe ici -->
</p>
```

## Transcrire un paragraphe avec alinéa : `<p @rend="indent">`
<rouge>mettre une image</rouge>

Le paragraphe doit être transcrit - comme tout paragraphe - au sein de la balise `<p>`. Mais on spécifiera un attribut à cette balise :
* attribut `@rend` dont la valeur doit être `indent`

```xml
<p rend="indent">
<!-- Transcrire le paragraphe ici -->
</p>
```

## Transcrire une rature : `@rend` de `<del>`
<rouge>mettre une image</rouge>

Au sein d'un paragraphe, une rature est transcrite avec l'élément `<del>`.
C'est l'attribut `@rend` qui permet d'indiquer comment cette rature prend forme :
* rature manuelle ou texte barré à la main : `@rend` prend la valeur `line-through`
* rature machine ou texte barré à la machine à écrire : `@rend` prend la valeur `overtyped`

```xml
<p rend="indent">
   (...)
   phrase contenant <del rend="line-through">une rature manuelle</del>
   (...)
   phrase contenant <del rend="overtyped">une rature machine</del>
   (...)
</p>
```


## Zone illisible ou difficilement déchiffrable : (`<gap>` ou ) `<unclear>`
<rouge>??? Un passage du texte (lettre, mot, suite de mots...) illisible, indéchiffrable est transcrit avec l'élément ??`<gap>` / <unclear/>??.</rouge>

Un passage du texte difficilement lisible mais pour lequel il est possible d'inférer ou de supposer une transcription est transcrit avec l'élément `<unclear>`.

```xml
<p rend="indent">
   (...)
   une phrase avec une portion illisible est annotée ainsi : <unclear>indiquer ici la lecture incertaine proposée</unclear>.
   (...)
   une phrase avec une portion illisible pour laquelle on ne propose pas de lecture incertaine est annotée ainsi : <unclear /> <gap/>.
   (...)
</p>
```

## Transcrire un soulignement : `<hi @rend="underline">`
<rouge>mettre une image</rouge>

Au sein d'un paragraphe, un passage souligné est transcrit avec l'élément `<hi>` qui porte l'attribut `@rend` dont la valeur doit être `underline`.

```xml
<p rend="indent">
   (...)
   phrase contenant <hi rend="underline">du texte souligné</del>
   (...)
</p>
```
