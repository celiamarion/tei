Nous proposons ici une liste non-exhaustive de logiciels de transcription. Pour chacun, nous tâchons d'indiquer si le logiciel est gratuit ou payant, sa licence, sous quel système d'exploitation il peut être installé... puis nous proposons une description succincte de ses caractéristiques.


### Oxygen XML Editor
* Logiciel payant
* Licence : [propre à Oxygen](https://www.oxygenxml.com/legal.html)
* Disponible sous : Windows, Mac et Linux
* Lien : [https://www.oxygenxml.com/](https://www.oxygenxml.com/)

Oxygen XML Editor est un logiciel d'édition de fichier XML. Il n'est pas dédié à la transcription, mais peut être utilisé à cette fin. Son gros avantage est la qualité de l'interface. Si elle peut sembler complexe au premier abord, elle s'avère vite efficace pour produire du XML de qualité et à fortiori suivre un standard, une DTD ou un schéma.
À noter : il possède une extension TEI et permet ainsi de créer des fichiers XML-TEI valides tout en bénéficiant d'aide à la sélection de balises et d'attributs et une interaction avec la documentation TEI.

La plupart des imprimes-écran de ce guide sont tirés du logiciel Oxygen.

### XXE
à compléter

### Etc.